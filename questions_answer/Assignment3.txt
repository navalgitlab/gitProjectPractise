Assignment3
1 Clone an existing repository on Github created during course and configure your local repo to point to the remote repository.
Ans
git clone https://github.com/NavalKishor/PractiseGitAndGitHub.git
cd PractiseGitAndGitHub
cd PractiseGitAndGitHub
ls
a.txt  c.txt  README.md

2 Perform some operation like add, remove, modify and finally push your changes to the remote repository
Ans
Create a new file run the command
git add <filename>
� Create few more directories and files inside the directory run the
git add --all
� To remove a file run the git rm <filename>
� To see the difference run the git diff <filename>
� Finally commit the code to the local repository using the
git commit -m "commit message"
To push the code to the repository run the command git push origin master

3 Pull the latest changes from the repository to get the updates from others in to your local repo and merge the changes
Ans

4 Try fetching the changes and perform the merge to get the difference between the pull and the merge command
Ans

5 Perform some changes and before committing the changes, stash your changes and then pull the changes and finally apply the changes to understand how stashing works
Ans

6 Create a feature branch and do some file operations in the branch and commit the changes to the branch
Ans

7 Merge the changes using the rebase command and finally perform a safe deletion of the feature branch
Ans

8 Create another feature branch and this time after committing the changes to the feature branch, merge the changes using fast forward merge and then delete the feature branch
Ans
