Problem Statement
Q1 Create an account on Github
Ans1 
	Created the Github account by following the a/c creation guideline of github
	and this is the public url for the github a/c https://github.com/NavalKishor

Q2 Fork the repository that will be created by the trainer for the project
Ans2 
	For the repositiry of the trainer name pradeep and his git_training repo to my account
	from url:https://github.com/prashdeep/git_training 
	to url:https://github.com/NavalKishor/git_training

Q3 Clone the forked repository on to local machine
Ans3 
	using git base i have clone the forked repository to my local machine
	following are the command i have used.
	step a) mkdir git_cert and cd git_cert
	step b) git clone https://github.com/NavalKishor/git_training.git
	step c) cd git_traning  and in this folder my local copy with master branch pointed is present.

Q4 Create feature branch on the local machine
Ans4 
	git branch feature_branch 
	git checkout feature_branch
	this command will create the feature branch for us and switch so we can work on it
	git checkout -b mybranch 
	this will  create and also switch to the newly created branch
	
Q5 Configure the upstream repo
Ans5
	git remote add upstream https://github.com/prashdeep/git_training.git
	by using this command we can add the upstream into our project

Q6 Add some files, edit the files. remove some files and finally commit the changes to the feature branch and push it to the origin repo
Ans6 
	case 1 add some file
	git checkout feature_branch
	touch fb_file_naval.txt
	vi fb_file_naval.txt
	here edit your file and add the content to the file and save it 
	git add fb_file_naval.txt
	git status
	git commit -m "fb_file_naval.txt added to the feature_branch"
	git push origin feature_branch

	case 2 add some more file 
	touch aFiletoadd.txt
	git add aFiletoadd.txt
	git commit -m "aFiletoadd.txt file is added"
	git push origin feature_branch

	case 3 add some more file 
	touch adelfile
	git add .
	git commit -m "adelfile added"
	git push origin feature_branch

	case 4 Modified a file and delete some file 
	Step a) ls   --to list all the file in the working directory
	Step b)echo "change in the content or modified file exmaple" >>aFiletoadd.txt
	--modified the content using above command
	Step c) git rm adelfile
	this will delete the file in the working directory
	Step d)git status   --if you know the status what are the thing i have done in working directory
	On branch feature_branch
	Changes to be committed:
	  (use "git reset HEAD <file>..." to unstage)

			deleted:    adelfile

	Changes not staged for commit:
	  (use "git add <file>..." to update what will be committed)
	  (use "git checkout -- <file>..." to discard changes in working directory)

			modified:   aFiletoadd.txt
	Step e)git add .
	Step f)git status
	warning: LF will be replaced by CRLF in aFiletoadd.txt.
	The file will have its original line endings in your working directory.
	On branch feature_branch
	Changes to be committed:
	  (use "git reset HEAD <file>..." to unstage)

			modified:   aFiletoadd.txt
			deleted:    adelfile

	Step g) git commit -m "aFiletoadd.txt is modified and adelfile is removed"
	Step h)git push origin feature_branch
	all your changes is been pushed to remote repo

Q7 Create a pull request for other developers
Ans7 
Now got to you local repo and update your master branch using the git command
	git pull origin master
	Already up-to-date.
	now create a feature branch using the git command
	git checkout -b feature_branch
	and do the changes you wana provide to this pull request so push this changes to remote repo as new branch using this git command.
	git push origin feature_branch
	as we have done in Q6,
	Now go to the github using following steps.
	 1.	Login to github and click on the project for which we want create the pull request
		https://github.com/NavalKishor/git_training
	 2.	Beside of Branch info there is button which "New Pull Request" click on it also select the right branch 
		where our commit is there.
	 3.	We will be navigated to Compare Changes and select the right branch where our commit is there and where 
		we need to merged  and down we will see a button "Create Pull Request" click it  
	 4.	We will be navigated to Open a pull request Here enter the tilte /summary  as "Feature branch" and 
		description as "pull request from Naval/git_traning master brnach to feature_branch" and leave a comment in the comment box. 
	 5.	Click down the "Create Pull Request" button and then it wil show you the pull request details.We have 
		created a pull request successfully.
	 
Q8 Work on the pull request feature and provide comments on Github UI
Ans8	
	Now go to the github using following steps.	
	1.	Login to github and click on the project for which we want create the pull request
		https://github.com/NavalKishor/git_training
	2.  Navigated to the project and cick on the  "Pull requests 1"
		if we have pull request it will display the number also like above 1 in "Pull requests 1" is telling us that we  have 1 pull request.
	3   After clicking we will have list of all pull request  and  also we can see that how many pull request is open  
		and closed.Now select a pull request by clicking the title of the pull request like 
		for us this is "naval.txt file modified".
		Here you can see the comment as well as leave you comment and also see the change in the file 
		so in our case i have give the comment and the file changes to get merge.		
		Here on this page if github we can view all the commits by all contained by a pull request under the commits tab,
		or see all the file changes from the pull request across all the commits under the "Files Changed" tab
	4   We can check the comment and status of pull request on this link
		https://github.com/NavalKishor/git_training/pull/2
		"ok" in the comment section before merged i have given the comment and then merged this pull request as 
		"NavalKishor merged commit 0c0d25f into master 2 hours ago"
		then after merge left a comment as 
		"ok pull request done"
		similarly with other branch i have created a pull request and merged it. 
		we also check the pending pull request on the upstream
		https://github.com/prashdeep/git_training/pulls
		https://github.com/prashdeep/git_training/pull/2
		https://github.com/prashdeep/git_training/pull/2/commits/1826882a611e017bcd98bae27b213fa9095c26ac

Q9 Merge the changes from the pull repository of other collaborators to their local repo
Ans9 
	1.	Login to github and click on the project for which we want create the pull request
		https://github.com/NavalKishor/git_training
	2.  Navigated to the project and cick on the  "Pull requests 1"
		if we have pull request it will display the number also like above 1 in "Pull requests 1" is telling us that we  have 1 pull request.
	3   After clicking we will have list of all pull request  and  also we can see that how many pull request is open  
		and closed.Now select a pull request by clicking the title of the pull request like 
		for us this is "naval.txt file modified".
		Here you can see the comment as well as leave you comment and also see the change in the file 
		so in our case i have give the comment and the file changes to get merge.		
		Here on this page if github we can view all the commits by all contained by a pull request under the commits tab,
		or see all the file changes from the pull request across all the commits under the "Files Changed" tab
	4   after clicking it will show you the details of changes in it and also merging option
		 like "Merge pull request","Squash and merge" and "Rebase and merge" by cicking the dropdown we will get other option which we can select as per our need. 
		 
	5   after merging it leave the comment so other developers to get an idea about it or you can close the
		pull request by leave the comment 

		Here the pull request related links
		request and comment: https://github.com/NavalKishor/git_training/pull/2
		merged file: https://github.com/NavalKishor/git_training/commit/0c0d25f2d7f2b602f9592fd4fee0356e09286d72
		
		(As i have created the request between our master and the feature branch and close it 5 time by merging it)
	6	and after that	just go to the you local repo and do the pull request by using the git command
		git pull origin master 
		if changes we want from  our repo in this case this is fine and it will pull all the change to our local repo
		if changes is coming from upstream then we need to use the git command
		git pull upstream master
		in any case if merge conflict will come then we need to manually resolve it and add it to local repo and commit it.
	

Q10 Push the changes to the main branch and finally delete the feature branches
Ans10 
	git checkout -b my_branch 
		Switched to a new branch 'my_branch'

	git branch
		feature_branch
		  master
		* my_branch	 
		to see if you are currently pointed to feature branch name my_branch with * as prefix
	 echo "new file name ab.txt is created. modified in my_branch" >>fileOne.txt
	 git add fileOne.txt
		 On branch my_branch
			Changes to be committed:
			  (use "git reset HEAD <file>..." to unstage)
			modified:   fileOne.txt
	 git commit -m "changes from my_branch"
		[my_branch warning: LF will be replaced by CRLF in fileOne.txt.
		The file will have its original line endings in your working directory.
		97ebb7f] changes from my_branch
		warning: LF will be replaced by CRLF in fileOne.txt.
		The file will have its original line endings in your working directory.
		 1 file changed, 1 insertion(+)
		 
	Going back to master branch by using the command 
	 git checkout master 
		Switched to branch 'master'
		Your branch is up-to-date with 'origin/master'.

	To merge the changes of my_branch to master branch use the git command 
	git merge my_branch 
		Merge made by the 'recursive' strategy.
		 aFiletoadd.txt    | 1 +
		 fb_file_naval.txt | 1 +
		 fileOne.txt       | 1 +
		 naval.txt         | 3 +++
		 4 files changed, 6 insertions(+)
		 create mode 100644 aFiletoadd.txt
		 create mode 100644 fb_file_naval.txt
		 create mode 100644 naval.txt
	As master branch have some pacth and other pull request which is committed and send for the merged so 
	here in the log we can see that git merged the changes using recursive strategy  
	
	Now we will pull all the changes from the master branch of remote repo using the command
	git	pull origin master 
		remote: Counting objects: 9, done.
		remote: Compressing objects: 100% (6/6), done.
		remote: Total 9 (delta 4), reused 8 (delta 3), pack-reused 0
		Unpacking objects: 100% (9/9), done.
		From https://github.com/NavalKishor/git_training
		   70952f4..8026e48  master     -> origin/master
		 * [new branch]      patch-1    -> origin/patch-1
		Already up-to-date!
		
	Now we can delete the my_branch as purpose of this branch is done so use the git command	
	git branch -d my_branch  
		Deleted branch my_branch (was 97ebb7f).
	
	git branch 
		 feature_branch
		* master
	To push to remote repo we will use this command
	git push origin master
		Counting objects: 6, done.
		Delta compression using up to 2 threads.
		Compressing objects: 100% (6/6), done.
		Writing objects: 100% (6/6), 747 bytes | 0 bytes/s, done.
		Total 6 (delta 3), reused 0 (delta 0)
		remote: Resolving deltas: 100% (3/3), completed with 1 local objects.
		To https://github.com/NavalKishor/git_training.git
		   8026e48..03d9171  master -> master
