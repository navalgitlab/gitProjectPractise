Assignment2
1 Install git on your computer and set up the global values for your user name and email
Ans
Download the git from this url https://git-scm.com/downloads
and install it on the local system
Right click the on mouse and select the option of git Base
window will popup and type this command to config the
gobal values for name and eamil
git config --global user.name "Naval Kishor Jha"
git config --global user.email er.navalkishor@yahoo.in

2 Configure system level and repository level values for user name and email address
Ans
Right click the on mouse and select the option of git Base
window will popup and type this command to config the
system values for name and eamil
git config --system user.name "Naval Kishor Jha"
git config --system user.email er.navalkishor@yahoo.in

3 Create a github account and create a repository
Ans
Created the Github account by following the a/c creation guideline of github
and this is the public url for the github a/c https://github.com/NavalKishor
there is + icon near your profile icon in the github a/c
click and select the new repository 
and give a name to the repo as i have given PractiseGitAndGitHub which u can check at this url https://github.com/NavalKishor/PractiseGitAndGitHub
give Description (optional) if you which wish
and select the scope or visibility of project either public or private based on the need here i have selected as public
Initialize this repository with a README this option check as tick 
and cick the button create repository  you are done.

4 Configure the local repo with the remote repository
Ans
git clone https://github.com/NavalKishor/PractiseGitAndGitHub.git
cd PractiseGitAndGitHub
ls 
total 1
-rw-r--r-- 1 Naval 197121 22 Mar 19 17:35 README.md  as you can see 1 file that is readme.md


5 Run the add, rm, diff, commit, push command to push the changes to the remote repository
Ans
i will create 3 empty file as bellow and added to local and commit and pushed to remote.
touch a.txt
touch b.txt
touch c.txt
git add .
git commit -m "a.txt,b.txt and c.txt files are added to project"
git push origin
-------------------------
there after the push to remote i am deleting the file name b.txt
git rm b.txt
here i am modifying the content of a.txt file 
echo "new file is good to start" >>a.txt
to see  the status of 
git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        deleted:    b.txt

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   a.txt


git diff
diff --git a/a.txt b/a.txt
index e69de29..7217246 100644
--- a/a.txt
+++ b/a.txt
@@ -0,0 +1 @@
+new file is good to start
warning: LF will be replaced by CRLF in a.txt.
The file will have its original line endings in your working directory.

git add .
git commit -m"deleted the file b.txt and modified the file a.txt"
git push origin